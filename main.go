package main

import (
	"database/sql"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"github.com/kshvakov/clickhouse"
)

const CH_HOST = "192.168.1.54"

func init() {
	rand.Seed(time.Now().UnixNano())
	// Create bd and table
	connect, err := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
	if err != nil {
		log.Fatal(err)
	}
	// Check ping
	if err := connect.Ping(); err != nil {
		if exception, ok := err.(*clickhouse.Exception); ok {
			fmt.Printf("[%d] %s \n%s\n", exception.Code, exception.Message, exception.StackTrace)
		} else {
			fmt.Println(err)
		}
		log.Fatal("unknown error")
	}
	// Create db
	if _, err := connect.Exec(CREATE_DB_SQL); err != nil {
		log.Fatal(err)
	}
	// Create table
	if _, err := connect.Exec(CREATE_TABLE_SQL); err != nil {
		log.Fatal(err)
	}
	// Create table with enum
	if _, err := connect.Exec(CREATE_TABLE_ENUM_SQL); err != nil {
		log.Fatal(err)
	}
	// Create table with array
	if _, err := connect.Exec(CREATE_TABLE_ARRAY_SQL); err != nil {
		log.Fatal(err)
	}
	if err := connect.Close(); err != nil {
		log.Fatal(err)
	}
}

func main() {
	resultDurationOne := oneConnect()
	println("one connect (1 conn), batch record: ", resultDurationOne.String())
	resultDurationMultimpe := multipleConnect()
	println("multiple connect (1 conn), batch record: ", resultDurationMultimpe.String())
	resultDurationOneEl := oneConnectEl()
	println("one connect (few conn), batch record: ", resultDurationOneEl.String())
	resultDurationMultimpeEl := multipleConnectEl()
	println("multiple connect (few conn), batch record: ", resultDurationMultimpeEl.String())
	/*resultDurationEnumOne := oneConnectEnum()
	println("one connect enum, batch record: ", resultDurationEnumOne.String())
	resultDurationEnumMultiple := multipleConnectEnum()
	println("multiple connect enum, batch record: ", resultDurationEnumMultiple.String())
	resultDurationArrayOne := oneConnectArray()
	println("one connect array, batch record: ", resultDurationArrayOne.String())
	resultDurationArrayMultiple := multipleConnectArray()
	println("multiple connect array, batch record: ", resultDurationArrayMultiple.String())*/
}

// UINT ===================================

func oneConnect() time.Duration {
	start := time.Now() // start timer
	// Connect
	connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
	for i := 0; i < 10; i++ {
		// Batch record
		var (
			tx, _   = connect.Begin()
			stmt, _ = tx.Prepare(INSERT_INTO_PREPARE)
		)
		for i := 0; i < 100000; i++ {
			if _, err := stmt.Exec(
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
			); err != nil {
				log.Fatal(err)
			}
		}
		if err := tx.Commit(); err != nil {
			log.Fatal(err)
		}
		stmt.Close()
	}
	connect.Close()
	return time.Now().Sub(start)
}

func multipleConnect() time.Duration {
	var wg sync.WaitGroup
	wg.Add(10)
	start := time.Now() // start timer

	// Connect
	connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true

	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			// Batch record
			var (
				tx, _   = connect.Begin()
				stmt, _ = tx.Prepare(INSERT_INTO_PREPARE)
			)
			defer stmt.Close()
			//defer connect.Close()
			// 250000
			for i := 0; i < 100000; i++ {
				if _, err := stmt.Exec(
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
				); err != nil {
					log.Fatal(err)
				}
			}
			if err := tx.Commit(); err != nil {
				log.Fatal(err)
			}
		}()
	}
	wg.Wait()
	connect.Close()
	return time.Now().Sub(start)
}

func oneConnectEl() time.Duration {
	start := time.Now() // start timer
	for i := 0; i < 10; i++ {
		// Connect
		connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
		// Batch record
		var (
			tx, _   = connect.Begin()
			stmt, _ = tx.Prepare(INSERT_INTO_PREPARE)
		)
		for i := 0; i < 100000; i++ {
			if _, err := stmt.Exec(
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
			); err != nil {
				log.Fatal(err)
			}
		}
		if err := tx.Commit(); err != nil {
			log.Fatal(err)
		}
		stmt.Close()
		connect.Close()
	}
	return time.Now().Sub(start)
}

func multipleConnectEl() time.Duration {
	var wg sync.WaitGroup
	wg.Add(1)
	start := time.Now() // start timer
	for i := 0; i < 10; i++ {
		go func() {
			defer wg.Done()
			// Connect
			connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
			defer connect.Close()
			// Batch record
			var (
				tx, _   = connect.Begin()
				stmt, _ = tx.Prepare(INSERT_INTO_PREPARE)
			)
			defer stmt.Close()
			//defer connect.Close()
			// 250000
			for i := 0; i < 100000; i++ {
				if _, err := stmt.Exec(
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
				); err != nil {
					log.Fatal(err)
				}
			}
			if err := tx.Commit(); err != nil {
				log.Fatal(err)
			}
		}()
	}
	wg.Wait()
	return time.Now().Sub(start)
}

// ARRAY ===================================

func oneConnectArray() time.Duration {
	start := time.Now() // start timer
	for i := 0; i < 5; i++ {
		// Connect
		connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
		// Batch record
		var (
			tx, _   = connect.Begin()
			stmt, _ = tx.Prepare(INSERT_INTO_ARRAY_PREPARE)
		)
		// 250000
		for i := 0; i < 1000000; i++ {
			if _, err := stmt.Exec(
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				[]uint8{1, 2, 3, 4},
				[]uint8{2, 3, 4},
				[]uint8{3, 4},
				[]uint8{4},
			); err != nil {
				log.Fatal(err)
			}
		}
		if err := tx.Commit(); err != nil {
			log.Fatal(err)
		}
		stmt.Close()
		connect.Close()
	}
	return time.Now().Sub(start)
}

func multipleConnectArray() time.Duration {
	var wg sync.WaitGroup
	wg.Add(5)
	start := time.Now() // start timer
	for i := 0; i < 5; i++ {
		go func() {
			defer wg.Done()
			// Connect
			connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
			// Batch record
			var (
				tx, _   = connect.Begin()
				stmt, _ = tx.Prepare(INSERT_INTO_ARRAY_PREPARE)
			)
			defer stmt.Close()
			defer connect.Close()
			// 250000
			for i := 0; i < 1000000; i++ {
				if _, err := stmt.Exec(
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					[]uint8{1, 2, 3, 4},
					[]uint8{2, 3, 4},
					[]uint8{3, 4},
					[]uint8{4},
				); err != nil {
					log.Fatal(err)
				}
			}
			if err := tx.Commit(); err != nil {
				log.Fatal(err)
			}
		}()
	}
	wg.Wait()
	return time.Now().Sub(start)
}

// ENUM ===================================

func oneConnectEnum() time.Duration {
	start := time.Now() // start timer
	for i := 0; i < 5; i++ {
		// Connect
		connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
		// Batch record
		var (
			tx, _   = connect.Begin()
			stmt, _ = tx.Prepare(INSERT_INTO_ENUM_PREPARE)
		)
		// 250000
		for i := 0; i < 1000000; i++ {
			if _, err := stmt.Exec(
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				getRand(),
				"item1",
				"item2",
				"item3",
				"item4",
			); err != nil {
				log.Fatal(err)
			}
		}
		if err := tx.Commit(); err != nil {
			log.Fatal(err)
		}
		stmt.Close()
		connect.Close()
	}
	return time.Now().Sub(start)
}

func multipleConnectEnum() time.Duration {
	var wg sync.WaitGroup
	wg.Add(5)
	start := time.Now() // start timer
	for i := 0; i < 5; i++ {
		go func() {
			defer wg.Done()
			// Connect
			connect, _ := sql.Open("clickhouse", "tcp://"+CH_HOST+":9000") //?debug=true
			// Batch record
			var (
				tx, _   = connect.Begin()
				stmt, _ = tx.Prepare(INSERT_INTO_ENUM_PREPARE)
			)
			defer stmt.Close()
			defer connect.Close()
			// 250000
			for i := 0; i < 1000000; i++ {
				if _, err := stmt.Exec(
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					getRand(),
					"item1",
					"item2",
					"item3",
					"item4",
				); err != nil {
					log.Fatal(err)
				}
			}
			if err := tx.Commit(); err != nil {
				log.Fatal(err)
			}
		}()
	}
	wg.Wait()
	return time.Now().Sub(start)
}

func getRand() uint8 {
	return uint8(rand.Intn(255))
}
