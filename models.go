package main

var (
	NAME_DB          = `ch_test_db`
	NAME_TABLE       = `test_table`
	CREATE_DB_SQL    = `CREATE DATABASE IF NOT EXISTS ` + NAME_DB
	CREATE_TABLE_SQL = `CREATE TABLE IF NOT EXISTS ` + NAME_DB + `.` + NAME_TABLE + `(
	    f1 UInt8,
		f2 UInt8,
		f3 UInt8,
		f4 UInt8,
		f5 UInt8,
		f6 UInt8,
		f7 UInt8,
		f8 UInt8,
		f9 UInt8,
		cretaed Date DEFAULT today())
	ENGINE = MergeTree(cretaed, (f1,f5,f9),8192)`
	CREATE_TABLE_ENUM_SQL = `CREATE TABLE IF NOT EXISTS ` + NAME_DB + `.` + NAME_TABLE + `_enum (
	    f1 UInt8,
		f2 UInt8,
		f3 UInt8,
		f4 UInt8,
		f5 UInt8,
		f6 Enum8('item1' = 1, 'item2' = 2, 'item3' = 3, 'item4' = 4),
		f7 Enum8('item1' = 1, 'item2' = 2, 'item3' = 3, 'item4' = 4),
		f8 Enum8('item1' = 1, 'item2' = 2, 'item3' = 3, 'item4' = 4),
		f9 Enum8('item1' = 1, 'item2' = 2, 'item3' = 3, 'item4' = 4),
		cretaed Date DEFAULT today())
	ENGINE = MergeTree(cretaed, (f1,f5,f9),8192)`
	CREATE_TABLE_ARRAY_SQL = `CREATE TABLE IF NOT EXISTS ` + NAME_DB + `.` + NAME_TABLE + `_array (
	    f1 UInt8,
		f2 UInt8,
		f3 UInt8,
		f4 UInt8,
		f5 UInt8,
		f6 Array(UInt8), 
		f7 Array(UInt8),
		f8 Array(UInt8),
		f9 Array(UInt8),
		cretaed Date DEFAULT today())
	ENGINE = MergeTree(cretaed, (f1,f5,f9),8192)`
	INSERT_INTO_PREPARE       = `INSERT INTO ` + NAME_DB + `.` + NAME_TABLE + ` (f1,f2,f3,f4,f5,f6,f7,f8,f9) VALUES (?,?,?,?,?,?,?,?,?)`
	INSERT_INTO_ENUM_PREPARE  = `INSERT INTO ` + NAME_DB + `.` + NAME_TABLE + `_enum (f1,f2,f3,f4,f5,f6,f7,f8,f9) VALUES (?,?,?,?,?,?,?,?,?)`
	INSERT_INTO_ARRAY_PREPARE = `INSERT INTO ` + NAME_DB + `.` + NAME_TABLE + `_array (f1,f2,f3,f4,f5,f6,f7,f8,f9) VALUES (?,?,?,?,?,?,?,?,?)`
)
